setwd("/Users/bowmanr/Projects/LS/HuMu/rawData")
load("Homologs.RData")
Mouse <- "10090"
Human <- "9606"
homologs.species <- homologs[homologs[,"Tax"]==Mouse|homologs[,"Tax"]==Human,]

HuMu<- read.delim("/Users/bowmanr/Projects/LS/HuMu/rawData/HuMu_annotation.txt",sep="\t",header=FALSE)
source("/Users/bowmanr/Projects/LS/Scripts/HuMu.mouse.gene.symbol.correction.R")
source("/Users/bowmanr/Projects/LS/Scripts/HuMu.human.gene.symbol.correction.R")
source("/Users/bowmanr/Projects/LS/Scripts/HuMu.Gene.lists.R")
setwd("/Users/bowmanr/Projects/LS/HuMu/Analysis")

load("normal.results.mouse.RData")
load("normal.results.human.RData")

HuMu.set<-intersect(HuMu[,2],c(rownames(results.mouse),rownames(results.human)))

missing.genes<-rbind(missing.genes.human,missing.genes.mouse)
Genes <- as.matrix(unique(HuMu.set))
Genes.to.rename<-as.matrix(Genes[Genes%in%missing.genes[,1]])
Genes.retain<-Genes[!Genes%in%missing.genes[,1]]

Gene.renamed<-as.character(merge(Genes.to.rename,missing.genes,by.x="V1",by.y="V1")[,2])

Genes <- c(Genes.retain,Gene.renamed)


Found<-Genes[Genes %in% homologs.species[,"Symbol"]]
Not.Found <-  Genes[!Genes %in% homologs.species[,"Symbol"]]

Set.1<-(homologs.species[(homologs.species[,"Symbol"]) %in%Genes,])

Set<-(homologs.species[homologs.species[,"Group"]%in%unique(Set.1[,"Group"]),])

Single<-names(table(Set[,"Group"]))[which(table(Set[,"Group"])==1)]
Double<-names(table(Set[,"Group"]))[which(table(Set[,"Group"])==2)]

Double.set<-Set[which(Set[,"Group"]%in%Double),]
Double.set.genes <- as.character(Double.set[,"Symbol"])
Single.set<-Set[which(Set[,"Group"]%in%Single),]
Single.set.genes <- as.character(Single.set[,"Symbol"])

B <- matrix(0,ncol=2,nrow=1)
for(i in unique(Double.set[,"Group"])){
	A<-t(as.matrix((Double.set[Double.set[,"Group"]==i,])[,"Symbol"]))
	B<- rbind(B,A)}

Validated.Homologs<-B[-1,]
Validated.Homologs.order <- Validated.Homologs[order(Validated.Homologs[,1], decreasing=FALSE),]

Other<-names(table(Set[,"Group"]))[which(table(Set[,"Group"])!=1&
										 table(Set[,"Group"])!=2)]
Other.set<-Set[which(Set[,"Group"]%in%Other),]

Other.set.genes <- as.character(Other.set[,"Symbol"])
Single.set.order <-Single.set[order(Single.set[,4], decreasing=FALSE),]
Not.found.order <- Not.Found[order(Not.Found,decreasing=FALSE)]

setwd("/Users/bowmanr/Projects/LS/Misc")
write.table(Single.set.order,"Single.set.homologs.txt",sep="\t")
write.table(Other.set,"Other.set.homologs.txt",sep="\t")
write.table(Not.found.order,"Not.found.set.homologs.txt",sep="\t")
write.table(Validated.Homologs.order,"Validated.Homologs.order.txt",sep="\t")


Group<-Set[grep(Gene,Set$Symbol),]$Group
Set[grep(Group,Set$Group),]

