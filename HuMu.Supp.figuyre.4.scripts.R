setwd("/Users/bowmanr/Projects/LS/HuMu/Analysis/")
A.1 <-	read.delim("Table.Wald.test.order.brain.FINAL.txt", sep="\t")  		#[,"Bone.p"]
B.1 <-	read.delim("Table.Wald.test.order.bone.FINAL.txt"          , sep="\t")	#[,"Bone.p"]
C.1 <-	read.delim("Table.Wald.test.order.lung.LM2.FINAL.txt"  , sep="\t")	#[,"Brain.p"]

D.1 <- read.delim("Table.Wald.test.order.FINAL.txt",sep="\t")


length(unique(c(rownames(A.1),rownames(B.1),rownames(C.1))))

Brain.tumor<-unique(c(
		rownames(A.1)[A.1[,"Brain.p"]<=0.05])  )  

Bone.tumor<-unique(c(
		rownames(B.1)[B.1[,"Bone.p"]<=0.05])   )	 	

Lung.tumor<-unique(c(
		rownames(C.1)[C.1[,"Lung.p"]<=0.05]))

brain.length<-length(Brain.tumor)
bone.length<-length(Bone.tumor)
lung.length<-length(Lung.tumor)

brain.bone<-intersect(Brain.tumor,Bone.tumor)
brain.lung<-intersect(Brain.tumor,Lung.tumor)
lung.bone<-intersect(Bone.tumor,Lung.tumor)
middle<-intersect(intersect(Bone.tumor,Lung.tumor),Brain.tumor)

brain.alone <- brain.length- length(brain.bone) -length(brain.lung) +1
bone.alone <- bone.length- length(brain.bone) -length(lung.bone) +1
lung.alone <- lung.length- length(brain.lung) -length(lung.bone) +1


brain.list <- setdiff(Brain.tumor,Lung.tumor)
brain.list <- setdiff(Brain.tumor,brain.bone)


Test.list<-list(c(Bone.tumor),c(Brain.tumor),c(Lung.tumor))

intersect(
	intersect(Test.list[[2]],Test.list[[3]])
	,Test.list[[1]])

library(VennDiagram)


venn.diagram(
	x = list(
		A = Test.list[[2]],
		B =  Test.list[[1]],
		C =  Test.list[[3]]
		),
	category.names= c("Brain",
					"Bone",
					"Lung"),
	margin=0.1,
	height=1500,
	width=1500,
	euler.d=FALSE,
	scaled=FALSE,
	fontfamily = "helvetica",
	filename = paste("Survival.VennDiagram_supertest.tiff",sep=""),
	col = "black",lty = 1,lwd = 1,
	fill = c("Green", "Blue", "Orange"),alpha = 0.50,cex=0.8,
	label.col = c(rep("black",7)),
	cat.pos = c(-38,38,180), cat.dist = c(0.065,0.065,0.05), cat.cex = 1,
	cat.col = "black",cat.fontfamily = "helvetica",
	main.fontfamily= "helvetica", main.cex=1,
	cat.default.pos = "outer",
	main=c("Survival Related genes")
	);





library(Vennerable)
tissue.lists <- Venn(Test.list)


######################
### Libraries used
######################
library(Hmisc)
library(survival)
library(coin)
library(matrixStats)
library(beeswarm)
library(ggplot2)

#####################
##  Data Import
#####################
setwd("/Users/bowmanr/Projects/LS/publicDataSets")

GSE12276.genes.exprs.data <- read.delim("GSE12276.gene.level.data.txt")
Clinical.info.brm.samples <- read.delim("Final.anootated.txt")
Clinical.info.brm.samples.1 <- Clinical.info.brm.samples[,c(2,5,6,7,8,9,10)]


####################
## Color Scheme
####################
setwd("/Users/bowmanr/Projects/LS/HuMu/Analysis")
palette() #default
new<- c("grey","black","Green","Orange","Blue")
palette(new) # Set palette to new color scheme

####################
##  Gene Selection
####################
Genes <-  Bone.tumor

###############################
##  Expresssion data Subsetting
###############################
Genes <- intersect(Genes,rownames(GSE12276.genes.exprs.data))
Test<-GSE12276.genes.exprs.data
Test.genes <- Test[Genes,]
Test.genes <-Test.genes[!is.na(Test.genes[,1]),]
Test.genes <- (Test.genes- rowMeans(Test.genes))/(rowSds(Test.genes))
Test.genes.t <- t(Test.genes)
Final <- merge(Clinical.info.brm.samples.1,Test.genes.t, by.y="row.names", by.x="Row.names")



Test.1 <- data.frame()
for(i in 8:c(length(Genes)+7)){
     Final$Group <- Final[,i]
     Test        <- t(as.matrix(as.matrix(summary(coxph(Surv(Metastasis.free.survival, Bone)~Group, 
                                                    data=Final))$conf.int)[c(1,3,4)]))
     Bone.p<-as.matrix(((summary(coxph(Surv(Metastasis.free.survival, Bone)~Group, data=Final)))$waldtest))[3,1]
     Test <- cbind(Test,Bone.p)
     colnames(Test)<-c("HR","CI.1","CI.2","P.Value")
     Test.1<-rbind(Test.1,Test)
}
rownames(Test.1) <- colnames(Final)[8:c(length(Genes)+7)]; Test.1$Gene <- rownames(Test.1)
Test.2 <- Test.1[order(Test.1$HR,decreasing=TRUE),]
Test.2$Gene <- factor(Test.2$Gene, levels=c(Test.2$Gene))
Test.3 <- signif(Test.2[,c(1,2,3,4)],4)
Test.3[,5] <- paste("(",Test.3[,2]," - ",Test.3[,3],")",sep="")
Test.4 <- Test.3[,c(1,5,4)] ; colnames(Test.4) <- c("Hazard Ratio","95% CI","P Value")
write.table(Test.4,"SF4.table.bone.hr_2.txt",sep="\t")
pdf(file="./SF4.bone.hr_italics_2.pdf",4,5)
ggplot(Test.2, aes(x=Gene, y=HR, ymin=CI.1, ymax=CI.2)) + 
        geom_errorbar(width=0.4)+
        geom_point(size=3, color="blue") +  
        labs(axis.text.x=element_text(face="bold.italic"))+
        coord_flip() +
        geom_hline(yintercept=1, lty=5, alpha=0.9,color="grey69") + theme_bw()+
        theme(axis.text.y=element_text(face='italic'))

dev.off()







####################
##  Gene Selection
####################
Genes <- Brain.tumor

###############################
##  Expresssion data Subsetting
###############################
Genes <- intersect(Genes,rownames(GSE12276.genes.exprs.data))
Test<-GSE12276.genes.exprs.data
Test.genes <- Test[Genes,]
Test.genes <-Test.genes[!is.na(Test.genes[,1]),]
Test.genes <- (Test.genes- rowMeans(Test.genes))/(rowSds(Test.genes))
Test.genes.t <- t(Test.genes)
Final <- merge(Clinical.info.brm.samples.1,Test.genes.t, by.y="row.names", by.x="Row.names")


Test.1 <- data.frame()
for(i in 8:c(length(Genes)+7)){
     Final$Group <- Final[,i]
     Test        <- t(as.matrix(as.matrix(summary(coxph(Surv(Metastasis.free.survival, Brain)~Group, 
                                                    data=Final))$conf.int)[c(1,3,4)]))
     Brain.p<-as.matrix(((summary(coxph(Surv(Metastasis.free.survival, Brain)~Group, data=Final)))$waldtest))[3,1]
     Test <- cbind(Test,Brain.p)
     colnames(Test)<-c("HR","CI.1","CI.2","P.Value")
     Test.1<-rbind(Test.1,Test)
}
rownames(Test.1) <- colnames(Final)[8:c(length(Genes)+7)]; Test.1$Gene <- rownames(Test.1)
Test.2 <- Test.1[order(Test.1$HR,decreasing=TRUE),]
Test.2$Gene <- factor(Test.2$Gene, levels=c(Test.2$Gene))
Test.3 <- signif(Test.2[,c(1,2,3,4)],4)
Test.3[,5] <- paste("(",Test.3[,2]," - ",Test.3[,3],")",sep="")
Test.4 <- Test.3[,c(1,5,4)] ; colnames(Test.4) <- c("Hazard Ratio","95% CI","P Value")
write.table(Test.4,"SF4.table.brain.hr_2.txt",sep="\t")
pdf(file="./SF4.brain.hr_italics_2.pdf",4,5)
ggplot(Test.2, aes(x=Gene, y=HR, ymin=CI.1, ymax=CI.2)) + 
        geom_errorbar(width=0.4)+
        geom_point(size=3, color="green") +  
        coord_flip() +
        geom_hline(yintercept=1, lty=5, alpha=0.9,color="grey69") + theme_bw()+
        theme(axis.text.y=element_text(face='italic'))

dev.off()





####################
##  Gene Selection
####################
Genes <- Lung.tumor

###############################
##  Expresssion data Subsetting
###############################
Genes <- intersect(Genes,rownames(GSE12276.genes.exprs.data))
Test<-GSE12276.genes.exprs.data
Test.genes <- Test[Genes,]
Test.genes <-Test.genes[!is.na(Test.genes[,1]),]
Test.genes <- (Test.genes- rowMeans(Test.genes))/(rowSds(Test.genes))
Test.genes.t <- t(Test.genes)
Final <- merge(Clinical.info.brm.samples.1,Test.genes.t, by.y="row.names", by.x="Row.names")


Test.1 <- data.frame()
for(i in 8:c(length(Genes)+7)){
     Final$Group <- Final[,i]
     Test        <- t(as.matrix(as.matrix(summary(coxph(Surv(Metastasis.free.survival, Lung)~Group, 
                                                    data=Final))$conf.int)[c(1,3,4)]))
     Lung.p<-as.matrix(((summary(coxph(Surv(Metastasis.free.survival, Lung)~Group, data=Final)))$waldtest))[3,1]
     Test <- cbind(Test,Lung.p)
     colnames(Test)<-c("HR","CI.1","CI.2","P.Value")
     Test.1<-rbind(Test.1,Test)
}
rownames(Test.1) <- colnames(Final)[8:c(length(Genes)+7)]; Test.1$Gene <- rownames(Test.1)
Test.2 <- Test.1[order(Test.1$HR,decreasing=TRUE),]
Test.2$Gene <- factor(Test.2$Gene, levels=c(Test.2$Gene))
Test.3 <- signif(Test.2[,c(1,2,3,4)],4)
Test.3[,5] <- paste("(",Test.3[,2]," - ",Test.3[,3],")",sep="")
Test.4 <- Test.3[,c(1,5,4)] ; colnames(Test.4) <- c("Hazard Ratio","95% CI","P Value")
write.table(Test.4,"SF4.table.lung.hr_2.txt",sep="\t")
pdf(file="SF4.lung.hr_italics_2.pdf",4,9.5)
ggplot(Test.2, aes(x=Gene, y=HR, ymin=CI.1, ymax=CI.2)) + 
        geom_errorbar(width=0.4)+
        geom_point(size=3, color="orange") +  
        coord_flip() +
        geom_hline(yintercept=1, lty=5, alpha=0.9,color="grey69") + theme_bw()+
        theme(axis.text.y=element_text(face='italic'))

dev.off()

intersect(Brain.tumor,Bone.tumor)
intersect(Brain.tumor,Lung.tumor)
intersect(Lung.tumor,Bone.tumor)


venn.diagram(
    x = list(
        A = c(Brain.tumor),
        B =  c(Bone.tumor),
        C =  c(Lung.tumor)
        ),
    category.names= c("Brain",
                    "Bone",
                    "Lung"),
    margin=0.1,
    height=1500,
    width=1500,
    euler.d=FALSE,
    scaled=FALSE,
    fontfamily = "helvetica",
    filename = paste("Sf4.venn.diagram.tiff",sep=""),
    col = "black",lty = 1,lwd = 1,
    fill = c("Green", "Blue", "Orange"),alpha = 0.50,cex=0.8,
    label.col = c(rep("black",7)),
    cat.pos = c(-38,38,180), cat.dist = c(0.065,0.065,0.05), cat.cex = 1,
    cat.col = "black",cat.fontfamily = "helvetica",
    main.fontfamily= "helvetica", main.cex=1,
    cat.default.pos = "outer",
    main=c("Stroma early vs late stage")
    );
dev.off()

