library(limma)
library(VennDiagram)
library(marray)
library(gplots)

setwd("/Users/bowmanr/Projects/LS/HuMu/Analysis")
load("./fc.tables.complete.RData")
load("exprs.tables.RData")
setwd("/Users/bowmanr/Projects/LS/HuMu/rawData")
Pheno<-read.delim("Phenodata.txt"); rownames(Pheno)<- Pheno[,"CEL"]

mouse <- exprs.table[[1]]
human <- exprs.table[[2]]
human <- fc.tables.complete[[2]]

Pheno.m <- Pheno[colnames(mouse),]
mouse.stage <- factor(Pheno[colnames(mouse),"Stage"])
mouse.tissue <- factor(Pheno[colnames(mouse),"Cell.Type"])

Pheno.h <- Pheno[colnames(human),]
human.stage <- factor(Pheno[colnames(human),"Stage"])
human.tissue <- factor(Pheno[colnames(human),"Cell.Type"])



design.mouse <- as.matrix(model.matrix(~0+mouse.tissue))
design.mouse.1 <-cbind(design.mouse[,2],design.mouse[,3], as.matrix(design.mouse[,1]+design.mouse[,4]+design.mouse[,5]))
colnames(design.mouse.1) <- c("bone","brain","lung")
fit.mouse <- lmFit(mouse, design.mouse.1);
contrast.mouse <- makeContrasts(lung-bone,lung-brain,bone-brain,  levels=design.mouse.1)

fit.mouse.2 <- contrasts.fit(fit.mouse, contrast.mouse)
fit.mouse.2 <- eBayes(fit.mouse.2)
fit.mouse.mat.2 <- data.frame(fit.mouse.2)
fit.mouse.mat.2["Hp",]

results.mouse <- as.matrix(decideTests(fit.mouse.2 , lfc=log2(2),
                                 adjust.method="fdr",p.value=0.1))
summary(results.mouse)

lung.list2 <- rownames(subset(results.mouse, results.mouse[,1]==1
												&
											results.mouse[,2]==1
												&
											results.mouse[,3]==0	))



bone.list2 <- rownames(subset(results.mouse, results.mouse[,1]==-1
												&
											results.mouse[,2]==0
												&
											results.mouse[,3]==1	))


brain.list2 <- rownames(subset(results.mouse, results.mouse[,1]==0
												&
											results.mouse[,2]==-1
												&
											results.mouse[,3]==-1	))
############
###  H
############


human.stage.1 <- droplevels(human.stage[human.stage!="ctrl"])#&human.stage!="late"]
human.tissue.1 <- human.tissue[human.stage!="ctrl"]#&human.stage!="late"]
human.1 <- human[,human.stage!="ctrl"]#&human.stage!="late"]

design.human <- as.matrix(model.matrix(~0+human.stage.1:human.tissue.1))
colnames(design.human) <- c("early.ATCC","late.ATCC","early.bone","late.bone",
							"early.brain","late.brain","early.LM2","late.LM2")


fit.human <- lmFit(human.1, design.human);
contrast.human <- makeContrasts(late.ATCC-early.ATCC,late.bone-early.bone,
							late.brain-early.brain, late.LM2-early.LM2, levels=design.human)

fit.human.2 <- contrasts.fit(fit.human, contrast.human)
fit.human.2 <- eBayes(fit.human.2)
fit.human.mat.2 <- data.frame(fit.human.2)
fit.human.mat.2["HP",]

results.human <- as.matrix(decideTests(fit.human.2 , lfc=log2(2),
                                 adjust.method="fdr",p.value=0.1))
summary(results.human)




lung.list2 <- rownames(subset(results.human, results.human[,1]==1
												&
											results.human[,2]==1
												&
											results.human[,3]==0
												&	
											results.human[,5]==-1	
												&	
											results.human[,6]==-1))

lung.ATCC.list2 <- rownames(subset(results.human, 
										#	results.human[,3]==0
										#		&	
											results.human[,5]==-1	
												&	
											results.human[,6]==-1))

lung.LM2.list2 <- rownames(subset(results.human,
											#results.human[,3]==0 
										    #	&
										    results.human[,1]==1
												&
											results.human[,2]==1))

bone.list2 <- rownames(subset(results.human, results.human[,1]==-1
												&
											results.human[,4]==1
												&
											results.human[,5]==1	))


brain.list2 <- rownames(subset(results.human, results.human[,2]==-1
												&
											results.human[,4]==-1
												&
											results.human[,6]==1	))

Set <- unique(c(lung.list2,lung.list2,bone.list2,brain.list2))



